<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
    <id>com.dec05eba.gpu_screen_recorder</id>
    <name>GPU Screen Recorder</name>
    <summary>A shadowplay-like screen recorder for Linux. The fastest screen recorder for Linux</summary>
    <developer_name>dec05eba</developer_name>
    <metadata_license>CC0-1.0</metadata_license>
    <project_license>GPL-3.0</project_license>
    <url type="homepage">https://git.dec05eba.com/gpu-screen-recorder/about/</url>
    <url type="bugtracker">https://github.com/dec05eba/gpu-screen-recorder-issues/issues</url>
    <url type="contribute">https://git.dec05eba.com/?p=about</url>
    <url type="donation">https://git.dec05eba.com/?p=about</url>

    <supports>
        <control>pointing</control>
        <control>keyboard</control>
    </supports>

    <description>
        <p>
            This is a screen recorder that has minimal impact on system performance by recording a monitor using the GPU only, similar to shadowplay on windows. This is the fastest screen recording tool for Linux. This screen recorder works on X11 and Wayland on AMD, Intel and NVIDIA.
        </p>
        <p>
            This screen recorder can be used for recording your desktop offline, for live streaming and for nvidia-like instant replay, where only the last few minutes are saved.
        </p>
        <p>Supported video codecs:</p>
        <ul>
            <li>H264 (default)</li>
            <li>HEVC (Optionally with HDR)</li>
            <li>AV1 (Optionally with HDR. Not currently supported on NVIDIA if you use GPU Screen Recorder flatpak)</li>
            <li>VP8</li>
            <li>VP9</li>
        </ul>
        <p>Supported audio codecs:</p>
        <ul>
            <li>Opus (default)</li>
            <li>AAC</li>
        </ul>
        <p>
            Recording a monitor requires (restricted) root access which means that you have to install GPU Screen Recorder system-wide: "flatpak install flathub --system com.dec05eba.gpu_screen_recorder"
            and pkexec needs to be installed on the system and a polkit agent needs to be running.
            Note that this only applies to when recording a monitor on AMD/Intel or when recording on Wayland without using the desktop portal option.
        </p>
        <p>AV1 is currently not supported in the flatpak for Nvidia since GPU Screen Recorder uses an older ffmpeg version to support older Nvidia cards. Install GPU Screen Recorder from source or from AUR if you want to use AV1 on Nvidia.</p>
        <p>
            Videos are in variable framerate format. Very out of date video players might have an issue playing such videos. It's recommend to use MPV to play such videos, otherwise you might experience stuttering in the video.
            You can select constant frame rate mode in advanced view if you need it.
        </p>
        <p>
            If the video doesn't play or you get green/yellow overlay then your video player is missing H264/HEVC video codec. Either install the video codecs or use mpv.
        </p>
        <p>AMD has a driver/hardware fault that causes black bars/distorted colors on the sides of the video for certain video resolutions. This happens for both av1 and hevc, so if you have this issue then switch to h264 video codec option in advanced settings.
        </p>
        <p>
            If H264/HEVC video encoding option is not available on your AMD/Intel system but you know that your GPU supports those codecs for encoding then you may need to install mesa-extra freedesktop runtime by running this command: "flatpak install --system org.freedesktop.Platform.GL.default//23.08-extra".
        </p>
        <p>
            On NVIDIA systems the flatpak NVIDIA driver needs to match the systems NVIDIA driver version or the NVIDIA driver will fail to properly load in flatpaks.
            Make sure you update flatpak on your system with: "flatpak update", or if that doesn't fix it you can try to install the specific flatpak NVIDIA driver version version that matches your systems NVIDIA driver version by running:
            "flatpak install org.freedesktop.Platform.GL.nvidia-version", for example "flatpak install org.freedesktop.Platform.GL.nvidia-550-54-14".
            You can find which NVIDIA driver version is running on your system by running "cat /proc/driver/nvidia/version".
        </p>
        <p>GPU Screen Recorder flatpak can install files in $HOME/.local/share/gpu-screen-recorder. If you want to uninstall GPU Screen Recorder then you will have to remove this directory manually.</p>
    </description>

    <launchable type="desktop-id">com.dec05eba.gpu_screen_recorder.desktop</launchable>
    <screenshots>
        <screenshot type="default">
            <caption>Simple view</caption>
            <image>https://raw.githubusercontent.com/dec05eba/com.dec05eba.gpu_screen_recorder/master/resources/screenshot1.png</image>
        </screenshot>
        <screenshot>
            <caption>Advanced view</caption>
            <image>https://raw.githubusercontent.com/dec05eba/com.dec05eba.gpu_screen_recorder/master/resources/screenshot2.png</image>
        </screenshot>
        <screenshot>
            <caption>Recording page</caption>
            <image>https://raw.githubusercontent.com/dec05eba/com.dec05eba.gpu_screen_recorder/master/resources/screenshot3.png</image>
        </screenshot>
    </screenshots>

    <releases>
        <release version="4.1.8" date="2024-09-17">
            <description>
                <ul>
                    <li>Improve video capture sync, making the video smoother. The video is smoother on X11 than Wayland</li>
                    <li>Improve video recording performance on AMD when the system usage is 100%</li>
                </ul>
            </description>
        </release>
        <release version="4.1.7" date="2024-09-06">
            <description>
                <ul>
                    <li>Fix tearing/stutter/cursor flicker on amd after recent amd driver update</li>
                </ul>
            </description>
        </release>
        <release version="4.1.6" date="2024-08-30">
            <description>
                <ul>
                    <li>Workaround steam deck driver bug and enable steam deck support again</li>
                    <li>Improve startup time on certain systems</li>
                </ul>
            </description>
        </release>
        <release version="4.1.5" date="2024-08-20">
            <description>
                <ul>
                    <li>Disable support for steam deck at the moment because steam deck drivers are broken</li>
                    <li>Improve quality again</li>
                </ul>
            </description>
        </release>
        <release version="4.1.4" date="2024-08-20">
            <description>
                <ul>
                    <li>Reduce video file size</li>
                </ul>
            </description>
        </release>
        <release version="4.1.3" date="2024-08-18">
            <description>
                <ul>
                    <li>Fix capture not working on wayland when recording a monitor instead of desktop portal</li>
                </ul>
            </description>
        </release>
        <release version="4.1.2" date="2024-08-17">
            <description>
                <ul>
                    <li>Allow capturing external monitors on a laptop with a dedicated gpu on X11 (for example an intel laptop with a nvidia gpu), where the monitor is connected to the dedicated gpu</li>
                    <li>Fix capture not working if audio device id has space in it (happens with virtual audio sources)</li>
                    <li>Add 10 bit color depth options for hevc and av1 to reduce banding</li>
                    <li>Fix cursor not visible when using multiple monitors in some wayland compositors</li>
                </ul>
            </description>
        </release>
        <release version="4.1.1" date="2024-08-15">
            <description>
                <ul>
                    <li>Fix error when starting application on wayland when portal capture option is available but laptop monitor is disabled</li>
                </ul>
            </description>
        </release>
        <release version="4.1.0" date="2024-08-08">
            <description>
                <ul>
                    <li>Fix possible lag on monitor/desktop portal capture</li>
                </ul>
            </description>
        </release>
        <release version="4.0.1" date="2024-07-23">
            <description>
                <ul>
                    <li>Fix capture broken on amd on wayland</li>
                    <li>Fix hdr capture causing crash when in replay mode</li>
                </ul>
            </description>
        </release>
        <release version="4.0.0" date="2024-07-22">
            <description>
                <ul>
                    <li>Added desktop portal (pipewire) capture option. This fixes issue with glitched capture on certain Intel iGPUS (on Wayland)</li>
                    <li>Added global hotkeys on Wayland. KDE Plasma is the only Wayland environment that supports this properly at the moment</li>
                    <li>Add separate hotkeys for start and stop and option to show notification when starting/stopping recording</li>
                    <li>Fix HDR capture (HDR metadata is now correct). Note that HDR capture is only available on Wayland and when recording a monitor without the desktop portal option</li>
                    <li>Added VP8 and VP9 video codecs if supported by the hardware</li>
                    <li>Added software encoding option</li>
                </ul>
            </description>
        </release>
        <release version="3.8.2" date="2024-06-22">
            <description>
                <ul>
                    <li>Default to h264 video codec because of amd driver issues (black bar) and for better compatibility with software</li>
                </ul>
            </description>
        </release>
        <release version="3.8.1" date="2024-06-16">
            <description>
                <ul>
                    <li>Properly cutout cursor outside video area when dealing with AMD HEVC padding driver bug</li>
                </ul>
            </description>
        </release>
        <release version="3.8.0" date="2024-06-10">
            <description>
                <ul>
                    <li>Add system tray icon</li>
                    <li>Fix screen recording on Intel ARC GPU</li>
                    <li>Workaround AMD driver bug with HEVC video codec that causes glitched graphics on the right/bottom side with certain video resolutions. It's now replaced with black color</li>
                    <li>Fix possible incorrect monitor rotation when using multiple monitors with one monitor rotated on GNOME Wayland</li>
                    <li>Add hls (m3u8) container option</li>
                </ul>
            </description>
        </release>
        <release version="3.7.7" date="2024-05-29">
            <description>
                <ul>
                    <li>Fix recording freeze on nvidia when __GL_SYNC_TO_VBLANK environment variable is set to 1</li>
                </ul>
            </description>
        </release>
        <release version="3.7.6" date="2024-05-20">
            <description>
                <ul>
                    <li>Do not force constant framerate for live streams</li>
                </ul>
            </description>
        </release>
        <release version="3.7.5" date="2024-05-19">
            <description>
                <ul>
                    <li>Fix twitch/youtube streaming not working</li>
                    <li>Fix possible freeze on recording stop if stopping quickly after starting recording</li>
                </ul>
            </description>
        </release>
        <release version="3.7.4" date="2024-05-13">
            <description>
                <p>Make audio sync even better. Audio sync is broken in all applications anyways so what can be done really</p>
            </description>
        </release>
        <release version="3.7.3" date="2024-05-12">
            <description>
                <ul>
                    <li>Re-enable opus audio codec</li>
                    <li>Remove flac audio codec option until it's fixed</li>
                    <li>Improve video quality when recording HDR</li>
                    <li>Fix flv issues</li>
                    <li>Add mpegts container and option to select codec for custom streaming service</li>
                </ul>
            </description>
        </release>
        <release version="3.7.2" date="2024-04-20">
            <description>
                <ul>
                    <li>Improve nvidia video encoding performance a lot on certain GPUs</li>
                    <li>Improve audio/video sync</li>
                    <li>Increase audio bitrate</li>
                    <li>Hide notifications after a few seconds</li>
                </ul>
            </description>
        </release>
        <release version="3.7.1" date="2024-04-12">
            <description>
                <p>Fix audio sync regression when using mixed audio</p>
            </description>
        </release>
        <release version="3.7.0" date="2024-04-11">
            <description>
                <p>Fix possible audio desync after some time for some users</p>
            </description>
        </release>
        <release version="3.6.5" date="2024-04-10">
            <description>
                <p>Fix nvidia x11 monitor capture breaking after suspend/monitor reconfiguration (even after applying nvidia preserve video memory on suspend)</p>
            </description>
        </release>
        <release version="3.6.4" date="2024-04-06">
            <description>
                <p>Fix recording breaking when updating flatpak on some systems (where $HOME is symlinked to another location), requires password prompt update</p>
            </description>
        </release>
        <release version="3.6.3" date="2024-03-21">
            <description>
                <ul>
                    <li>Fix nvidia window capture/wayland capture if the user has set __GL_THREADED_OPTIMIZATIONS to 1</li>
                    <li>Fix full color range capture in nvidia window capture</li>
                    <li>Properly clear cursor in window capture</li>
                </ul>
            </description>
        </release>
        <release version="3.6.2" date="2024-03-20">
            <description>
                <ul>
                    <li>Fix incorrect overclocking offset on some nvidia gpus when enabling overclocking option</li>
                    <li>Ignore color standards for more accurate colors</li>
                </ul>
            </description>
        </release>
        <release version="3.6.1" date="2024-03-11">
            <description>
                <p>Add the option to not record cursor</p>
            </description>
        </release>
        <release version="3.6.0" date="2024-03-10">
            <description>
                <ul>
                    <li>Support HDR capture and full color range on nvidia</li>
                    <li>Support cursor capture when recording a single window</li>
                    <li>Show the correct monitors when using prime-run</li>
                </ul>
            </description>
        </release>
        <release version="3.5.3" date="2024-02-16">
            <description>
                <p>Fix minor permissions issue</p>
            </description>
        </release>
        <release version="3.5.2" date="2024-02-14">
            <description>
                <p>Better error message for missing h264/hevc, ignore user nvidia vaapi setting</p>
            </description>
        </release>
        <release version="3.5.1" date="2024-02-11">
            <description>
                <ul>
                    <li>Fix crash on x11 nvidia when recording monitor</li>
                    <li>Fix color issue when recording window on amd/intel</li>
                </ul>
            </description>
        </release>
        <release version="3.5.0" date="2024-02-11">
            <description>
                <ul>
                    <li>Support monitor rotation</li>
                    <li>Support hdr capture on amd/intel wayland (currently missing hdr metadata because of driver bugs)</li>
                    <li>Default to variable framerate videos on nvidia x11 as well</li>
                    <li>Center capture window/follow focused and clear background</li>
                    <li>Remove 'remove password prompts' button. Only ask for password once and never again</li>
                </ul>
            </description>
        </release>
        <release version="3.4.2" date="2024-01-27">
            <description>
                <ul>
                    <li>Support hardware cursor plane capture on nvidia wayland</li>
                    <li>Add record timer</li>
                </ul>
            </description>
        </release>
        <release version="3.4.1" date="2024-01-17">
            <description>
                <ul>
                    <li>Improve audio drifting in some cases</li>
                    <li>Fix audio stuttering with pulseaudio + opus/flac + multiple audio sources merged</li>
                    <li>Add option to remove hotkey with backspace</li>
                    <li>Decide which audio device is default output/input when starting to record</li>
                </ul>
            </description>
        </release>
        <release version="3.4.0" date="2024-01-07">
            <description>
                <p>Add option to pause recording</p>
            </description>
        </release>
        <release version="3.3.2" date="2023-12-31">
            <description>
                <p>Fix monitor capture on AMD/Intel or NVIDIA Wayland on some distros, such as OpenSUSE</p>
            </description>
        </release>
        <release version="3.3.1" date="2023-12-02">
            <description>
                <ul>
                    <li>Fix opus and flac</li>
                    <li>Fix crash when live streaming without an audio source</li>
                    <li>Fix missing streaming show/hide key symbol</li>
                </ul>
            </description>
        </release>
        <release version="3.3.0" date="2023-12-01">
            <description>
                <ul>
                    <li>Add option to remove password prompts (if possible on the system)</li>
                    <li>Add experimental av1 support (if supported by the hardware). Currently only enabled for AMD/Intel in the flatpak.</li>
                    <li>Configure quality settings to reduce file size on amd/intel</li>
                    <li>Workaround amd/intel driver issue when using h264 (or fps > 60) and mkv (forcefully set video codec to hevc)</li>
                </ul>
            </description>
        </release>
        <release version="3.2.6" date="2023-11-27">
            <description>
                <p>Fix possible crash when recording multiple audio sources (merged)</p>
            </description>
        </release>
        <release version="3.2.5" date="2023-11-18">
            <description>
                <p>Fix incorrect color on some nvidia wayland systems running kde plasma (support 10-bit color depth)</p>
            </description>
        </release>
        <release version="3.2.4" date="2023-11-05">
            <description>
                <p>Nvidia x11: disable screen direct record option, as there is an nvidia driver bug that causes some games to freeze/crash with this option</p>
            </description>
        </release>
        <release version="3.2.3" date="2023-11-01">
            <description>
                <p>Use opengl context instead of egl2, might fix program startup not working for some users</p>
            </description>
        </release>
        <release version="3.2.2" date="2023-10-31">
            <description>
                <p>Intel: attempt to fix glitched graphics on some intel gpus. I dont know, ask intel why they are weird</p>
            </description>
        </release>
        <release version="3.2.1" date="2023-10-22">
            <description>
                <p>AMD/Intel wayland: fix broken colors when system uses 10-bit</p>
            </description>
        </release>
        <release version="3.2.0" date="2023-10-21">
            <description>
                <ul>
                    <li>AMD/Intel: workaround driver bug that causes vram leak</li>
                    <li>Add frame rate mode option in advanced menu</li>
                    <li>Redesign audio input to make it clear that you have to add audio to use it</li>
                </ul>
            </description>
        </release>
        <release version="3.1.7" date="2023-08-15">
            <description>
                <p>NVIDIA: Workaround new nvidia driver bug that causes recording of a small window to freeze recording.</p>
            </description>
        </release>
        <release version="3.1.6" date="2023-08-13">
            <description>
                <p>AMD/Intel: fix capture issue on some systems</p>
            </description>
        </release>
        <release version="3.1.5" date="2023-07-25">
            <description>
                <p>AMD/Intel: Fix too dark video in the flatpak version.</p>
            </description>
        </release>
        <release version="3.1.4" date="2023-07-23">
            <description>
                <p>AMD/Intel: Better color accuracy.</p>
            </description>
        </release>
        <release version="3.1.3" date="2023-07-22">
            <description>
                <p>Fix capture on wlroots based wayland compositors. Fix cursor offset in amd/intel capture.</p>
            </description>
        </release>
        <release version="3.1.2" date="2023-07-22">
            <description>
                <p>Support cursor capture on AMD/Intel on Wayland. Attempt to fix multi monitor offset capture in some cases.</p>
            </description>
        </release>
        <release version="3.1.1" date="2023-07-21">
            <description>
                <p>Support cursor capture on AMD/Intel on Wayland.</p>
            </description>
        </release>
        <release version="3.1.0" date="2023-07-21">
            <description>
            <ul>
                <li>Support wlroots capture when possible (no kms root access required)</li>
                <li>Make window capture a bit more robust</li>
                <li>Fix possible lag on recording start on AMD/Intel</li>
                <li>Make capture on AMD/Intel possible without Xwayland</li>
            </ul>
            </description>
        </release>
        <release version="3.0.0" date="2023-07-18">
            <description>
                <p>Experimental wayland support on AMD/Intel/NVIDIA. Hotkeys not supported.</p>
            </description>
        </release>
        <release version="2.2.0" date="2023-07-10">
            <description>
                <p>Attempt to fix screen recording when multiple graphics cards are connected</p>
            </description>
        </release>
        <release version="2.1.7" date="2023-07-07">
            <description>
                <p>Show proper error when running on wayland. Preparing for wayland support.</p>
            </description>
        </release>
        <release version="2.1.6" date="2023-06-10">
            <description>
                <p>AMD/Intel: fix multi monitor capture coordinate being incorrect for some GPU driver versions.</p>
            </description>
        </release>
        <release version="2.1.5" date="2023-05-22">
            <description>
                <p>Attempt to fix a recent nvidia (cuda) driver bug on RTX cards that causes a freeze when stopping recording.</p>
            </description>
        </release>
        <release version="2.1.4" date="2023-05-17">
            <description>
                <p>Fix issue where the application freezes and keyboard freezes when selecting a new hotkey and another widget is clicked.</p>
            </description>
        </release>
        <release version="2.1.3" date="2023-05-12">
            <description>
                <p>Attempt to fix possible issue on some systems with amd/intel where capture region is incorrect when multiple monitors are connected.</p>
            </description>
        </release>
        <release version="2.1.2" date="2023-04-27">
            <description>
                <p>Attempt to fix possible audio/video sync on NVIDIA after recording for a long time. Temporary disable opus/flac because it breaks when recording multiple audio inputs.</p>
            </description>
        </release>
        <release version="2.1.1" date="2023-04-22">
            <description>
                <p>AMD/Intel: Add support for capturing cursor when recording a monitor. Fix some cases of capture being glitched when using multiple monitors.</p>
            </description>
        </release>
        <release version="2.1.0" date="2023-04-18">
            <description>
                <p>Enable AMD/Intel monitor capture. Requires the flatpak to be installed system-wide and it requires restricted root access. Record a single window if you dont like these restrictions. Videos created on AMD/Intel should be played with MPV otherwise it might have issues such as stuttering.</p>
            </description>
        </release>
        <release version="2.0.0" date="2023-04-11">
            <description>
                <p>Add experimental support for AMD/Intel. Quality might not be well tuned yet and the video is in variable framerate mode, which might cause issues with some out of date video editing software or video players. Recording on AMD/intel is currently limited to a window (monitor capture is not possible yet).</p>
            </description>
        </release>
        <release version="1.3.5" date="2023-03-22">
            <description>
            <p>Show error when using wayland (because wayland is not supported)</p>
            </description>
        </release>
        <release version="1.3.4" date="2023-03-17">
            <description>
            <p>Add option to workaround a NVIDIA driver "bug" that causes framerate to drop a bit when recording (overclock memory transfer rate back to normal)</p>
            </description>
        </release>
        <release version="1.3.3" date="2023-03-11">
            <description>
            <p>Make it clear when graphics card is not supported</p>
            </description>
        </release>
        <release version="1.3.2" date="2023-03-04">
            <description>
            <ul>
                <li>Add VRR record option, only use with VRR as it might have driver issues!</li>
                <li>Add opus/flac audio options. opus/flac is only supported by .mp4/.mkv. Automatically changes audio codec if not supported by the container</li>
            </ul>
            </description>
        </release>
        <release version="1.3.1" date="2023-02-22">
            <description>
            <p>Fix broken replay when recording with audio (possibly broken in normal recording as well)</p>
            </description>
        </release>
        <release version="1.3.0" date="2023-02-20">
            <description>
            <ul>
                <li>Switch to EGL (fixes possible window capture issues when using a compositor)</li>
                <li>Add option to change hotkeys</li>
                <li>Add option to merge audio tracks into one audio track</li>
                <li>Add option to follow the focused window</li>
                <li>Add option to force set h264/hevc (services such as discord can't play hevc videos directly in the application)</li>
                <li>Show proper error when NVIDIA GPU is not in use</li>
            </ul>
            </description>
        </release>
        <release version="1.2.1" date="2022-11-24">
            <description>
            <p>Allow choosing between mp4, flv and mkv for record/replay. mkv survives system crashes</p>
            </description>
        </release>
        <release version="1.2.0" date="2022-10-27">
            <description>
            <p>Re-enable screen-direct, disable h264 forced fallback and use p6 again</p>
            </description>
        </release>
        <release version="1.1.8" date="2022-10-18">
            <description>
            <p>Attempt to fix stuttering in video that can happen sometimes (especially when vsync is enabled)</p>
            </description>
        </release>
        <release version="1.1.7" date="2022-10-16">
            <description>
            <p>Properly fallback to h264 if hevc is not supported by the gpu</p>
            </description>
        </release>
        <release version="1.1.6" date="2022-10-15">
            <description>
            <p>Fix livestreaming: flv does not properly fallback to h264</p>
            </description>
        </release>
        <release version="1.1.5" date="2022-10-11">
            <description>
            <p>Use lower preset on older gpus (kepler) and switch to h264 if h265 is not supported</p>
            </description>
        </release>
        <release version="1.1.4" date="2022-10-11">
            <description>
            <p>Tune options to try and reduce file size</p>
            </description>
        </release>
        <release version="1.1.3" date="2022-10-08">
            <description>
            <p>Better tune quality options for different resolutions</p>
            </description>
        </release>
        <release version="1.1.2" date="2022-10-07">
            <description>
            <p>Fix crash caused by invalid memory write when recording audio</p>
            </description>
        </release>
        <release version="1.1.1" date="2022-10-05">
            <description>
            <p>Fix possible pulseaudio crash</p>
            </description>
        </release>
        <release version="1.1.0" date="2022-10-05">
            <description>
            <p>Fixes some streaming issues (mostly metadata missing)</p>
            </description>
        </release>
        <release version="1.0.0" date="2022-09-30"/>
    </releases>
    <content_rating type="oars-1.1"/>
</component>
